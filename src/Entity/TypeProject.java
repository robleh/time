/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author 440Z3
 */
@Entity
@Table(name = "type_project", catalog = "timeG", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeProject.findAll", query = "SELECT t FROM TypeProject t"),
    @NamedQuery(name = "TypeProject.findByIdtp", query = "SELECT t FROM TypeProject t WHERE t.idtp = :idtp"),
    @NamedQuery(name = "TypeProject.findByName", query = "SELECT t FROM TypeProject t WHERE t.name = :name")})
public class TypeProject implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idtp")
    private Integer idtp;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idtp")
    private Collection<Project> projectCollection;

    public TypeProject() {
    }

    public TypeProject(Integer idtp) {
        this.idtp = idtp;
    }

    public TypeProject(Integer idtp, String name) {
        this.idtp = idtp;
        this.name = name;
    }

    public Integer getIdtp() {
        return idtp;
    }

    public void setIdtp(Integer idtp) {
        this.idtp = idtp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<Project> getProjectCollection() {
        return projectCollection;
    }

    public void setProjectCollection(Collection<Project> projectCollection) {
        this.projectCollection = projectCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtp != null ? idtp.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeProject)) {
            return false;
        }
        TypeProject other = (TypeProject) object;
        if ((this.idtp == null && other.idtp != null) || (this.idtp != null && !this.idtp.equals(other.idtp))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Controller.TypeProject[ idtp=" + idtp + " ]";
    }
    
}
