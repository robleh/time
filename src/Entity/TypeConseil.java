/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author 440Z3
 */
@Entity
@Table(name = "type_conseil", catalog = "timeG", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeConseil.findAll", query = "SELECT t FROM TypeConseil t"),
    @NamedQuery(name = "TypeConseil.findByIdtc", query = "SELECT t FROM TypeConseil t WHERE t.idtc = :idtc"),
    @NamedQuery(name = "TypeConseil.findByName", query = "SELECT t FROM TypeConseil t WHERE t.name = :name")})
public class TypeConseil implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idtc")
    private Integer idtc;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idtc")
    private Collection<Conseil> conseilCollection;

    public TypeConseil() {
    }

    public TypeConseil(Integer idtc) {
        this.idtc = idtc;
    }

    public TypeConseil(Integer idtc, String name) {
        this.idtc = idtc;
        this.name = name;
    }

    public Integer getIdtc() {
        return idtc;
    }

    public void setIdtc(Integer idtc) {
        this.idtc = idtc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlTransient
    public Collection<Conseil> getConseilCollection() {
        return conseilCollection;
    }

    public void setConseilCollection(Collection<Conseil> conseilCollection) {
        this.conseilCollection = conseilCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idtc != null ? idtc.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeConseil)) {
            return false;
        }
        TypeConseil other = (TypeConseil) object;
        if ((this.idtc == null && other.idtc != null) || (this.idtc != null && !this.idtc.equals(other.idtc))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Controller.TypeConseil[ idtc=" + idtc + " ]";
    }
    
}
