
package Entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Robleh
 */
@Entity
@Table(name = "fonctionality", catalog = "timeG", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Fonctionality.findAll", query = "SELECT f FROM Fonctionality f"),
    @NamedQuery(name = "Fonctionality.findAllByproject", query = "SELECT f FROM Fonctionality f WHERE f.idprojet = :idprojet"),
    @NamedQuery(name = "Fonctionality.findByIdfonc", query = "SELECT f FROM Fonctionality f WHERE f.idfonc = :idfonc"),
    @NamedQuery(name = "Fonctionality.findByName", query = "SELECT f FROM Fonctionality f WHERE f.name = :name"),
    @NamedQuery(name = "Fonctionality.findByDescription", query = "SELECT f FROM Fonctionality f WHERE f.description = :description"),
    @NamedQuery(name = "Fonctionality.findByDateHeurDebut", query = "SELECT f FROM Fonctionality f WHERE f.dateHeurDebut = :dateHeurDebut"),
    @NamedQuery(name = "Fonctionality.findByfinish", query = "SELECT f FROM Fonctionality f WHERE f.finish = :finish"),
    @NamedQuery(name = "Fonctionality.findByDateHeurFin", query = "SELECT f FROM Fonctionality f WHERE f.dateHeurFin = :dateHeurFin")})
public class Fonctionality implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idfonc")
    private Integer idfonc;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "finish")
    private String finish;
    @Basic(optional = false)
    @Column(name = "date_heur_debut")
    private String dateHeurDebut;
    @Basic(optional = false)
    @Column(name = "date_heur_fin")
    private String dateHeurFin;
    @JoinColumn(name = "idprojet", referencedColumnName = "idprojet")
    @ManyToOne(optional = false)
    private Project idprojet;

    public Fonctionality() {
    }

    public Fonctionality(Integer idfonc) {
        this.idfonc = idfonc;
    }

    public Fonctionality(Integer idfonc, String name, String description, String dateHeurDebut, String dateHeurFin, Project idprojet) {
        this.idfonc = idfonc;
        this.name = name;
        this.description = description;
        this.dateHeurDebut = dateHeurDebut;
        this.dateHeurFin = dateHeurFin;
        this.idprojet = idprojet;
    }
    public Fonctionality(Integer idfonc, String name, String description, String dateHeurDebut, String dateHeurFin, Project idprojet,String finish) {
        this.idfonc = idfonc;
        this.name = name;
        this.description = description;
        this.dateHeurDebut = dateHeurDebut;
        this.dateHeurFin = dateHeurFin;
        this.idprojet = idprojet;
        this.finish = finish;
    }

    public String getFinish() {
        return finish;
    }

    public void setFinish(String finish) {
        this.finish = finish;
    }

    
    
    public Integer getIdfonc() {
        return idfonc;
    }

    public void setIdfonc(Integer idfonc) {
        this.idfonc = idfonc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateHeurDebut() {
        return dateHeurDebut;
    }

    public void setDateHeurDebut(String dateHeurDebut) {
        this.dateHeurDebut = dateHeurDebut;
    }

    public String getDateHeurFin() {
        return dateHeurFin;
    }

    public void setDateHeurFin(String dateHeurFin) {
        this.dateHeurFin = dateHeurFin;
    }

    public Project getIdprojet() {
        return idprojet;
    }

    public void setIdprojet(Project idprojet) {
        this.idprojet = idprojet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idfonc != null ? idfonc.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fonctionality)) {
            return false;
        }
        Fonctionality other = (Fonctionality) object;
        if ((this.idfonc == null && other.idfonc != null) || (this.idfonc != null && !this.idfonc.equals(other.idfonc))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Controller.Fonctionality[ idfonc=" + idfonc + " ]";
    }
    
}
