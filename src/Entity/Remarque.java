/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 440Z3
 */
@Entity
@Table(name = "remarque", catalog = "timeG", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Remarque.findAll", query = "SELECT r FROM Remarque r"),
    @NamedQuery(name = "Remarque.findByIdremarque", query = "SELECT r FROM Remarque r WHERE r.idremarque = :idremarque"),
    @NamedQuery(name = "Remarque.findByName", query = "SELECT r FROM Remarque r WHERE r.name = :name"),
    @NamedQuery(name = "Remarque.findByDescription", query = "SELECT r FROM Remarque r WHERE r.description = :description")})
public class Remarque implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idremarque")
    private Integer idremarque;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @JoinColumn(name = "idprojet", referencedColumnName = "idprojet")
    @ManyToOne(optional = false)
    private Project idprojet;

    public Remarque() {
    }

    public Remarque(Integer idremarque) {
        this.idremarque = idremarque;
    }

    public Remarque(Integer idremarque, String name) {
        this.idremarque = idremarque;
        this.name = name;
    }

    public Integer getIdremarque() {
        return idremarque;
    }

    public void setIdremarque(Integer idremarque) {
        this.idremarque = idremarque;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Project getIdprojet() {
        return idprojet;
    }

    public void setIdprojet(Project idprojet) {
        this.idprojet = idprojet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idremarque != null ? idremarque.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Remarque)) {
            return false;
        }
        Remarque other = (Remarque) object;
        if ((this.idremarque == null && other.idremarque != null) || (this.idremarque != null && !this.idremarque.equals(other.idremarque))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Controller.Remarque[ idremarque=" + idremarque + " ]";
    }
    
}
