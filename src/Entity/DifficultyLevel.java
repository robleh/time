/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 440Z3
 */
@Entity
@Table(name = "difficulty_level", catalog = "timeG", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DifficultyLevel.findAll", query = "SELECT d FROM DifficultyLevel d"),
    @NamedQuery(name = "DifficultyLevel.findByIddf", query = "SELECT d FROM DifficultyLevel d WHERE d.iddf = :iddf"),
    @NamedQuery(name = "DifficultyLevel.findByName", query = "SELECT d FROM DifficultyLevel d WHERE d.name = :name"),
    @NamedQuery(name = "DifficultyLevel.findByPoint", query = "SELECT d FROM DifficultyLevel d WHERE d.point = :point")})
public class DifficultyLevel implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "iddf")
    private Integer iddf;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "point")
    private int point;

    public DifficultyLevel() {
    }

    public DifficultyLevel(Integer iddf) {
        this.iddf = iddf;
    }

    public DifficultyLevel(Integer iddf, String name, int point) {
        this.iddf = iddf;
        this.name = name;
        this.point = point;
    }

    public Integer getIddf() {
        return iddf;
    }

    public void setIddf(Integer iddf) {
        this.iddf = iddf;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (iddf != null ? iddf.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DifficultyLevel)) {
            return false;
        }
        DifficultyLevel other = (DifficultyLevel) object;
        if ((this.iddf == null && other.iddf != null) || (this.iddf != null && !this.iddf.equals(other.iddf))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Controller.DifficultyLevel[ iddf=" + iddf + " ]";
    }
    
}
