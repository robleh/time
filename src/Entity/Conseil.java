/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 440Z3
 */
@Entity
@Table(name = "conseil", catalog = "timeG", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Conseil.findAll", query = "SELECT c FROM Conseil c"),
    @NamedQuery(name = "Conseil.findByIdconseil", query = "SELECT c FROM Conseil c WHERE c.idconseil = :idconseil"),
    @NamedQuery(name = "Conseil.findByName", query = "SELECT c FROM Conseil c WHERE c.name = :name"),
    @NamedQuery(name = "Conseil.findByDescription", query = "SELECT c FROM Conseil c WHERE c.description = :description")})
public class Conseil implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idconseil")
    private Integer idconseil;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "description")
    private String description;
    @JoinColumn(name = "idtc", referencedColumnName = "idtc")
    @ManyToOne(optional = false)
    private TypeConseil idtc;
    @JoinColumn(name = "iduser", referencedColumnName = "iduser")
    @ManyToOne(optional = false)
    private User iduser;

    public Conseil() {
    }

    public Conseil(Integer idconseil) {
        this.idconseil = idconseil;
    }

    public Conseil(Integer idconseil, String name, String description) {
        this.idconseil = idconseil;
        this.name = name;
        this.description = description;
    }

    public Integer getIdconseil() {
        return idconseil;
    }

    public void setIdconseil(Integer idconseil) {
        this.idconseil = idconseil;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TypeConseil getIdtc() {
        return idtc;
    }

    public void setIdtc(TypeConseil idtc) {
        this.idtc = idtc;
    }

    public User getIduser() {
        return iduser;
    }

    public void setIduser(User iduser) {
        this.iduser = iduser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idconseil != null ? idconseil.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Conseil)) {
            return false;
        }
        Conseil other = (Conseil) object;
        if ((this.idconseil == null && other.idconseil != null) || (this.idconseil != null && !this.idconseil.equals(other.idconseil))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Controller.Conseil[ idconseil=" + idconseil + " ]";
    }
    
}
