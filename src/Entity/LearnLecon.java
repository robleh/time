/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author 440Z3
 */
@Entity
@Table(name = "learn_lecon", catalog = "timeG", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LearnLecon.findAll", query = "SELECT l FROM LearnLecon l"),
    @NamedQuery(name = "LearnLecon.findByIdll", query = "SELECT l FROM LearnLecon l WHERE l.idll = :idll"),
    @NamedQuery(name = "LearnLecon.findByName", query = "SELECT l FROM LearnLecon l WHERE l.name = :name"),
    @NamedQuery(name = "LearnLecon.findByDescription", query = "SELECT l FROM LearnLecon l WHERE l.description = :description"),
    @NamedQuery(name = "LearnLecon.findByDate", query = "SELECT l FROM LearnLecon l WHERE l.date = :date"),
    @NamedQuery(name = "LearnLecon.findByFinish", query = "SELECT l FROM LearnLecon l WHERE l.finish = :finish"),
    @NamedQuery(name = "LearnLecon.findByBegin", query = "SELECT l FROM LearnLecon l WHERE l.begin = :begin")})
public class LearnLecon implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idll")
    private Integer idll;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Column(name = "date")
    private String date;
    @Basic(optional = false)
    @Column(name = "finish")
    private String finish;
    @Basic(optional = false)
    @Column(name = "begin")
    private String begin;
    @JoinColumn(name = "iduser", referencedColumnName = "iduser")
    @ManyToOne(optional = false)
    private User iduser;

    public LearnLecon() {
    }

    public LearnLecon(Integer idll) {
        this.idll = idll;
    }

    public LearnLecon(Integer idll, String name, String finish, String begin) {
        this.idll = idll;
        this.name = name;
        this.finish = finish;
        this.begin = begin;
    }

    public Integer getIdll() {
        return idll;
    }

    public void setIdll(Integer idll) {
        this.idll = idll;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getFinish() {
        return finish;
    }

    public void setFinish(String finish) {
        this.finish = finish;
    }

    public String getBegin() {
        return begin;
    }

    public void setBegin(String begin) {
        this.begin = begin;
    }

    public User getIduser() {
        return iduser;
    }

    public void setIduser(User iduser) {
        this.iduser = iduser;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idll != null ? idll.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LearnLecon)) {
            return false;
        }
        LearnLecon other = (LearnLecon) object;
        if ((this.idll == null && other.idll != null) || (this.idll != null && !this.idll.equals(other.idll))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Controller.LearnLecon[ idll=" + idll + " ]";
    }
    
}
