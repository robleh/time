package Entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ROBLEH HOUSSEIN FARAH
 */
@Entity
@Table(name = "project", catalog = "timeG", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Project.findAll", query = "SELECT p FROM Project p"),
    @NamedQuery(name = "Project.findByIdprojet", query = "SELECT p FROM Project p WHERE p.idprojet = :idprojet"),
    @NamedQuery(name = "Project.findByName", query = "SELECT p FROM Project p WHERE p.name = :name"),
    @NamedQuery(name = "Project.findByDescription", query = "SELECT p FROM Project p WHERE p.description = :description"),
    @NamedQuery(name = "Project.findByDatedebut", query = "SELECT p FROM Project p WHERE p.datedebut = :datedebut"),
    @NamedQuery(name = "Project.findByDatefin", query = "SELECT p FROM Project p WHERE p.datefin = :datefin"),
    @NamedQuery(name = "Project.findByuser", query = "SELECT p FROM Project p WHERE p.iduser = :iduser"),
    @NamedQuery(name = "Project.findByFinish", query = "SELECT p FROM Project p WHERE p.finish = :finish")})
public class Project implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idprojet")
    private Integer idprojet;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @Column(name = "datedebut")
    private String datedebut;
    @Basic(optional = false)
    @Column(name = "datefin")
    private String datefin;
    @Basic(optional = false)
    @Column(name = "finish")
    private String finish;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idprojet")
    @JoinColumn(name = "iddf", referencedColumnName = "iddf")
    @ManyToOne(optional = false)
    private DifficultyLevel iddf;
    @JoinColumn(name = "idtp", referencedColumnName = "idtp")
    @ManyToOne(optional = false)
    private TypeProject idtp;
    @JoinColumn(name = "iduser", referencedColumnName = "iduser")
    @ManyToOne(optional = false)
    private User iduser;
    

    public Project() {
    }

    public Project(Integer idprojet) {
        this.idprojet = idprojet;
    }

    public Project(Integer idprojet, String name, String description ,String datedebut, String datefin, String finish,User user) {
        this.idprojet = idprojet;
        this.name = name;
        this.description = description;
        this.datedebut = datedebut;
        this.datefin = datefin;
        this.finish = finish;
        this.iduser = user;
    }

    public Project(Integer idprojet, String name, String description, String datedebut, String datefin, String finish, DifficultyLevel iddf, TypeProject idtp, User iduser) {
        this.idprojet = idprojet;
        this.name = name;
        this.description = description;
        this.datedebut = datedebut;
        this.datefin = datefin;
        this.finish = finish;
        this.iddf = iddf;
        this.idtp = idtp;
        this.iduser = iduser;
    }
    

    public Integer getIdprojet() {
        return idprojet;
    }

    public void setIdprojet(Integer idprojet) {
        this.idprojet = idprojet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDatedebut() {
        return datedebut;
    }

    public void setDatedebut(String datedebut) {
        this.datedebut = datedebut;
    }

    public String getDatefin() {
        return datefin;
    }

    public void setDatefin(String datefin) {
        this.datefin = datefin;
    }

    public String getFinish() {
        return finish;
    }

    public void setFinish(String finish) {
        this.finish = finish;
    }

    public DifficultyLevel getIddf() {
        return iddf;
    }

    public void setIddf(DifficultyLevel iddf) {
        this.iddf = iddf;
    }

    public TypeProject getIdtp() {
        return idtp;
    }

    public void setIdtp(TypeProject idtp) {
        this.idtp = idtp;
    }

    public User getIduser() {
        return iduser;
    }

    public void setIduser(User iduser) {
        this.iduser = iduser;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idprojet != null ? idprojet.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Project)) {
            return false;
        }
        Project other = (Project) object;
        if ((this.idprojet == null && other.idprojet != null) || (this.idprojet != null && !this.idprojet.equals(other.idprojet))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Controller.Project[ idprojet=" + idprojet + " ]";
    }
    
}
