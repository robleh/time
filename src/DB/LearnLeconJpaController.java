/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import DB.exceptions.NonexistentEntityException;
import Entity.LearnLecon;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author 440Z3
 */
public class LearnLeconJpaController implements Serializable {

    public LearnLeconJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(LearnLecon learnLecon) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(learnLecon);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(LearnLecon learnLecon) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            learnLecon = em.merge(learnLecon);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = learnLecon.getIdll();
                if (findLearnLecon(id) == null) {
                    throw new NonexistentEntityException("The learnLecon with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            LearnLecon learnLecon;
            try {
                learnLecon = em.getReference(LearnLecon.class, id);
                learnLecon.getIdll();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The learnLecon with id " + id + " no longer exists.", enfe);
            }
            em.remove(learnLecon);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<LearnLecon> findLearnLeconEntities() {
        return findLearnLeconEntities(true, -1, -1);
    }

    public List<LearnLecon> findLearnLeconEntities(int maxResults, int firstResult) {
        return findLearnLeconEntities(false, maxResults, firstResult);
    }

    private List<LearnLecon> findLearnLeconEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(LearnLecon.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public LearnLecon findLearnLecon(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(LearnLecon.class, id);
        } finally {
            em.close();
        }
    }

    public int getLearnLeconCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<LearnLecon> rt = cq.from(LearnLecon.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
