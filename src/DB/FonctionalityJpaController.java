/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import DB.exceptions.NonexistentEntityException;
import Entity.Fonctionality;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author 440Z3
 */
public class FonctionalityJpaController implements Serializable {

    public FonctionalityJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Fonctionality fonctionality) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(fonctionality);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Fonctionality fonctionality) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            fonctionality = em.merge(fonctionality);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = fonctionality.getIdfonc();
                if (findFonctionality(id) == null) {
                    throw new NonexistentEntityException("The fonctionality with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Fonctionality fonctionality;
            try {
                fonctionality = em.getReference(Fonctionality.class, id);
                fonctionality.getIdfonc();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The fonctionality with id " + id + " no longer exists.", enfe);
            }
            em.remove(fonctionality);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Fonctionality> findFonctionalityEntities() {
        return findFonctionalityEntities(true, -1, -1);
    }

    public List<Fonctionality> findFonctionalityEntities(int maxResults, int firstResult) {
        return findFonctionalityEntities(false, maxResults, firstResult);
    }

    private List<Fonctionality> findFonctionalityEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Fonctionality.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Fonctionality findFonctionality(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Fonctionality.class, id);
        } finally {
            em.close();
        }
    }

    public int getFonctionalityCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Fonctionality> rt = cq.from(Fonctionality.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
