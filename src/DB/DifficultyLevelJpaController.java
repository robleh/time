/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import DB.exceptions.NonexistentEntityException;
import Entity.DifficultyLevel;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author 440Z3
 */
public class DifficultyLevelJpaController implements Serializable {

    public DifficultyLevelJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(DifficultyLevel difficultyLevel) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(difficultyLevel);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(DifficultyLevel difficultyLevel) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            difficultyLevel = em.merge(difficultyLevel);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = difficultyLevel.getIddf();
                if (findDifficultyLevel(id) == null) {
                    throw new NonexistentEntityException("The difficultyLevel with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            DifficultyLevel difficultyLevel;
            try {
                difficultyLevel = em.getReference(DifficultyLevel.class, id);
                difficultyLevel.getIddf();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The difficultyLevel with id " + id + " no longer exists.", enfe);
            }
            em.remove(difficultyLevel);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<DifficultyLevel> findDifficultyLevelEntities() {
        return findDifficultyLevelEntities(true, -1, -1);
    }

    public List<DifficultyLevel> findDifficultyLevelEntities(int maxResults, int firstResult) {
        return findDifficultyLevelEntities(false, maxResults, firstResult);
    }

    private List<DifficultyLevel> findDifficultyLevelEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(DifficultyLevel.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public DifficultyLevel findDifficultyLevel(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(DifficultyLevel.class, id);
        } finally {
            em.close();
        }
    }

    public int getDifficultyLevelCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<DifficultyLevel> rt = cq.from(DifficultyLevel.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
