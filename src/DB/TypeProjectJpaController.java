/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import DB.exceptions.IllegalOrphanException;
import DB.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Entity.Project;
import Entity.TypeProject;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author 440Z3
 */
public class TypeProjectJpaController implements Serializable {

    public TypeProjectJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TypeProject typeProject) {
        if (typeProject.getProjectCollection() == null) {
            typeProject.setProjectCollection(new ArrayList<Project>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Project> attachedProjectCollection = new ArrayList<Project>();
            for (Project projectCollectionProjectToAttach : typeProject.getProjectCollection()) {
                projectCollectionProjectToAttach = em.getReference(projectCollectionProjectToAttach.getClass(), projectCollectionProjectToAttach.getIdprojet());
                attachedProjectCollection.add(projectCollectionProjectToAttach);
            }
            typeProject.setProjectCollection(attachedProjectCollection);
            em.persist(typeProject);
            for (Project projectCollectionProject : typeProject.getProjectCollection()) {
                TypeProject oldIdtpOfProjectCollectionProject = projectCollectionProject.getIdtp();
                projectCollectionProject.setIdtp(typeProject);
                projectCollectionProject = em.merge(projectCollectionProject);
                if (oldIdtpOfProjectCollectionProject != null) {
                    oldIdtpOfProjectCollectionProject.getProjectCollection().remove(projectCollectionProject);
                    oldIdtpOfProjectCollectionProject = em.merge(oldIdtpOfProjectCollectionProject);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TypeProject typeProject) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TypeProject persistentTypeProject = em.find(TypeProject.class, typeProject.getIdtp());
            Collection<Project> projectCollectionOld = persistentTypeProject.getProjectCollection();
            Collection<Project> projectCollectionNew = typeProject.getProjectCollection();
            List<String> illegalOrphanMessages = null;
            for (Project projectCollectionOldProject : projectCollectionOld) {
                if (!projectCollectionNew.contains(projectCollectionOldProject)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Project " + projectCollectionOldProject + " since its idtp field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Project> attachedProjectCollectionNew = new ArrayList<Project>();
            for (Project projectCollectionNewProjectToAttach : projectCollectionNew) {
                projectCollectionNewProjectToAttach = em.getReference(projectCollectionNewProjectToAttach.getClass(), projectCollectionNewProjectToAttach.getIdprojet());
                attachedProjectCollectionNew.add(projectCollectionNewProjectToAttach);
            }
            projectCollectionNew = attachedProjectCollectionNew;
            typeProject.setProjectCollection(projectCollectionNew);
            typeProject = em.merge(typeProject);
            for (Project projectCollectionNewProject : projectCollectionNew) {
                if (!projectCollectionOld.contains(projectCollectionNewProject)) {
                    TypeProject oldIdtpOfProjectCollectionNewProject = projectCollectionNewProject.getIdtp();
                    projectCollectionNewProject.setIdtp(typeProject);
                    projectCollectionNewProject = em.merge(projectCollectionNewProject);
                    if (oldIdtpOfProjectCollectionNewProject != null && !oldIdtpOfProjectCollectionNewProject.equals(typeProject)) {
                        oldIdtpOfProjectCollectionNewProject.getProjectCollection().remove(projectCollectionNewProject);
                        oldIdtpOfProjectCollectionNewProject = em.merge(oldIdtpOfProjectCollectionNewProject);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = typeProject.getIdtp();
                if (findTypeProject(id) == null) {
                    throw new NonexistentEntityException("The typeProject with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TypeProject typeProject;
            try {
                typeProject = em.getReference(TypeProject.class, id);
                typeProject.getIdtp();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The typeProject with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Project> projectCollectionOrphanCheck = typeProject.getProjectCollection();
            for (Project projectCollectionOrphanCheckProject : projectCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TypeProject (" + typeProject + ") cannot be destroyed since the Project " + projectCollectionOrphanCheckProject + " in its projectCollection field has a non-nullable idtp field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(typeProject);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TypeProject> findTypeProjectEntities() {
        return findTypeProjectEntities(true, -1, -1);
    }

    public List<TypeProject> findTypeProjectEntities(int maxResults, int firstResult) {
        return findTypeProjectEntities(false, maxResults, firstResult);
    }

    private List<TypeProject> findTypeProjectEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TypeProject.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TypeProject findTypeProject(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TypeProject.class, id);
        } finally {
            em.close();
        }
    }

    public int getTypeProjectCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TypeProject> rt = cq.from(TypeProject.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
