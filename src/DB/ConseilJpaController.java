/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import DB.exceptions.NonexistentEntityException;
import Entity.Conseil;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Entity.TypeConseil;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author 440Z3
 */
public class ConseilJpaController implements Serializable {

    public ConseilJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Conseil conseil) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TypeConseil idtc = conseil.getIdtc();
            if (idtc != null) {
                idtc = em.getReference(idtc.getClass(), idtc.getIdtc());
                conseil.setIdtc(idtc);
            }
            em.persist(conseil);
            if (idtc != null) {
                idtc.getConseilCollection().add(conseil);
                idtc = em.merge(idtc);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Conseil conseil) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Conseil persistentConseil = em.find(Conseil.class, conseil.getIdconseil());
            TypeConseil idtcOld = persistentConseil.getIdtc();
            TypeConseil idtcNew = conseil.getIdtc();
            if (idtcNew != null) {
                idtcNew = em.getReference(idtcNew.getClass(), idtcNew.getIdtc());
                conseil.setIdtc(idtcNew);
            }
            conseil = em.merge(conseil);
            if (idtcOld != null && !idtcOld.equals(idtcNew)) {
                idtcOld.getConseilCollection().remove(conseil);
                idtcOld = em.merge(idtcOld);
            }
            if (idtcNew != null && !idtcNew.equals(idtcOld)) {
                idtcNew.getConseilCollection().add(conseil);
                idtcNew = em.merge(idtcNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = conseil.getIdconseil();
                if (findConseil(id) == null) {
                    throw new NonexistentEntityException("The conseil with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Conseil conseil;
            try {
                conseil = em.getReference(Conseil.class, id);
                conseil.getIdconseil();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The conseil with id " + id + " no longer exists.", enfe);
            }
            TypeConseil idtc = conseil.getIdtc();
            if (idtc != null) {
                idtc.getConseilCollection().remove(conseil);
                idtc = em.merge(idtc);
            }
            em.remove(conseil);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Conseil> findConseilEntities() {
        return findConseilEntities(true, -1, -1);
    }

    public List<Conseil> findConseilEntities(int maxResults, int firstResult) {
        return findConseilEntities(false, maxResults, firstResult);
    }

    private List<Conseil> findConseilEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Conseil.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Conseil findConseil(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Conseil.class, id);
        } finally {
            em.close();
        }
    }

    public int getConseilCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Conseil> rt = cq.from(Conseil.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
