/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DB;

import DB.exceptions.IllegalOrphanException;
import DB.exceptions.NonexistentEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import Entity.Conseil;
import Entity.TypeConseil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author 440Z3
 */
public class TypeConseilJpaController implements Serializable {

    public TypeConseilJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(TypeConseil typeConseil) {
        if (typeConseil.getConseilCollection() == null) {
            typeConseil.setConseilCollection(new ArrayList<Conseil>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Collection<Conseil> attachedConseilCollection = new ArrayList<Conseil>();
            for (Conseil conseilCollectionConseilToAttach : typeConseil.getConseilCollection()) {
                conseilCollectionConseilToAttach = em.getReference(conseilCollectionConseilToAttach.getClass(), conseilCollectionConseilToAttach.getIdconseil());
                attachedConseilCollection.add(conseilCollectionConseilToAttach);
            }
            typeConseil.setConseilCollection(attachedConseilCollection);
            em.persist(typeConseil);
            for (Conseil conseilCollectionConseil : typeConseil.getConseilCollection()) {
                TypeConseil oldIdtcOfConseilCollectionConseil = conseilCollectionConseil.getIdtc();
                conseilCollectionConseil.setIdtc(typeConseil);
                conseilCollectionConseil = em.merge(conseilCollectionConseil);
                if (oldIdtcOfConseilCollectionConseil != null) {
                    oldIdtcOfConseilCollectionConseil.getConseilCollection().remove(conseilCollectionConseil);
                    oldIdtcOfConseilCollectionConseil = em.merge(oldIdtcOfConseilCollectionConseil);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(TypeConseil typeConseil) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TypeConseil persistentTypeConseil = em.find(TypeConseil.class, typeConseil.getIdtc());
            Collection<Conseil> conseilCollectionOld = persistentTypeConseil.getConseilCollection();
            Collection<Conseil> conseilCollectionNew = typeConseil.getConseilCollection();
            List<String> illegalOrphanMessages = null;
            for (Conseil conseilCollectionOldConseil : conseilCollectionOld) {
                if (!conseilCollectionNew.contains(conseilCollectionOldConseil)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Conseil " + conseilCollectionOldConseil + " since its idtc field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Collection<Conseil> attachedConseilCollectionNew = new ArrayList<Conseil>();
            for (Conseil conseilCollectionNewConseilToAttach : conseilCollectionNew) {
                conseilCollectionNewConseilToAttach = em.getReference(conseilCollectionNewConseilToAttach.getClass(), conseilCollectionNewConseilToAttach.getIdconseil());
                attachedConseilCollectionNew.add(conseilCollectionNewConseilToAttach);
            }
            conseilCollectionNew = attachedConseilCollectionNew;
            typeConseil.setConseilCollection(conseilCollectionNew);
            typeConseil = em.merge(typeConseil);
            for (Conseil conseilCollectionNewConseil : conseilCollectionNew) {
                if (!conseilCollectionOld.contains(conseilCollectionNewConseil)) {
                    TypeConseil oldIdtcOfConseilCollectionNewConseil = conseilCollectionNewConseil.getIdtc();
                    conseilCollectionNewConseil.setIdtc(typeConseil);
                    conseilCollectionNewConseil = em.merge(conseilCollectionNewConseil);
                    if (oldIdtcOfConseilCollectionNewConseil != null && !oldIdtcOfConseilCollectionNewConseil.equals(typeConseil)) {
                        oldIdtcOfConseilCollectionNewConseil.getConseilCollection().remove(conseilCollectionNewConseil);
                        oldIdtcOfConseilCollectionNewConseil = em.merge(oldIdtcOfConseilCollectionNewConseil);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = typeConseil.getIdtc();
                if (findTypeConseil(id) == null) {
                    throw new NonexistentEntityException("The typeConseil with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            TypeConseil typeConseil;
            try {
                typeConseil = em.getReference(TypeConseil.class, id);
                typeConseil.getIdtc();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The typeConseil with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            Collection<Conseil> conseilCollectionOrphanCheck = typeConseil.getConseilCollection();
            for (Conseil conseilCollectionOrphanCheckConseil : conseilCollectionOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This TypeConseil (" + typeConseil + ") cannot be destroyed since the Conseil " + conseilCollectionOrphanCheckConseil + " in its conseilCollection field has a non-nullable idtc field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            em.remove(typeConseil);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<TypeConseil> findTypeConseilEntities() {
        return findTypeConseilEntities(true, -1, -1);
    }

    public List<TypeConseil> findTypeConseilEntities(int maxResults, int firstResult) {
        return findTypeConseilEntities(false, maxResults, firstResult);
    }

    private List<TypeConseil> findTypeConseilEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(TypeConseil.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public TypeConseil findTypeConseil(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(TypeConseil.class, id);
        } finally {
            em.close();
        }
    }

    public int getTypeConseilCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<TypeConseil> rt = cq.from(TypeConseil.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
