package Controller;

import DB.FonctionalityJpaController;
import DB.ProjectJpaController;
import Entity.Fonctionality;
import Entity.Project;
import Entity.User;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * FXML Controller class
 *
 * @author ROBLEH HOUSSEIN FARAH
 */
public class MainFXMLController implements Initializable, ILoad {

    @FXML
    private Menu MenuEvolution;
    @FXML
    private MenuItem Menustatistic;
    @FXML
    private MenuItem Menurond;
    @FXML
    private MenuItem MenuExist;
    @FXML
    private Menu MenuEdit;
    @FXML
    private TextField txtsearch;
    @FXML
    private TableColumn<Project, String> colnameproject;
    @FXML
    private TableColumn<Project, String> coldescrproject;
    @FXML
    private TableColumn<Project, String> coldatedebutproject;
    @FXML
    private TableColumn<Project, String> coldatefinproject;
    @FXML
    private TableColumn<Project, String> colfinishproject;
    @FXML
    private TableColumn<Project, String> coltypeproject;
    @FXML
    private TableColumn<Project, String> coldifficultyproject;
    @FXML
    private TableColumn<Fonctionality, String> colnamefonct;
    @FXML
    private TableColumn<Fonctionality, String> coldescfonc;
    @FXML
    private TableColumn<Fonctionality, String> coldatedebutfonc;
    @FXML
    private TableColumn<Fonctionality, String> coldatefinfonc;
    @FXML
    private TableColumn<Fonctionality, String> colfinishfonc;
    @FXML
    private TableView<Project> tableProject;
    @FXML
    private TableView<Fonctionality> tablefunc;

    private User user = null;

    ObservableList<Project> projectItems = FXCollections.observableArrayList();

    ObservableList<Fonctionality> functItems = FXCollections.observableArrayList();

    private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("TimePU");

    private EntityManager em = emf.createEntityManager();

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        tableProject.getSelectionModel().selectedItemProperty().addListener((ObservableValue<? extends Project> observable, Project oldValue, Project newValue) -> {
            if (newValue != null) {
                System.out.println(newValue.getIdprojet());
                initTableFunc(newValue);
            }
        });
        ContextMenuProject();
        ContextMenuFonctionnality();
    }

    @FXML
    private void OnGraphStatistic(ActionEvent event) {
    }

    @FXML
    private void OnRange(ActionEvent event) {
    }

    @FXML
    private void OnExist(ActionEvent event) {
        System.exit(0);
    }

    @FXML
    private void OnAbout(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("TimeG");
        alert.setContentText("TimeG est une application qui est creer pour repondre à des besoins personnel(ex:virer le comportement paresseux des developpeurs),"
                + " j'arrivais pas gerer mon temps alors j'ai écris ce petit programme pour voir toutes les "
                + "taches que j'ai a faire entre tel et tel date/heur.... c'est la version BETA, vous trouverez le code source sur ce lien https://bitbucket.org/robleh/time");
        alert.show();
    }

    @FXML
    private void Onhelp(ActionEvent event) {
         Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("TimeG Help");
        alert.setContentText("TimeG est une application qui est creer pour repondre à des besoins personnel(ex:virer le comportement paresseux des developpeurs),"
                + " j'arrivais pas gerer mon temps alors j'ai écris ce petit programme pour voir toutes les "
                + "taches que j'ai a faire entre tel et tel date/heur.... c'est la version BETA, vous trouverez le code source sur ce lien https://bitbucket.org/robleh/time");
        alert.show();
    }

    @FXML
    private void txtsearch(ActionEvent event) {
    }

    @FXML
    private void OnRefresh(ActionEvent event) {
        System.out.println(user.getName());
        projectItems.clear();
        initTableProject(user);
        initTableFunc(null);
    }

    private void initTableProject(User u) {
        projectItems.clear();
        em.createNamedQuery("Project.findByuser", Project.class)
                .setHint("javax.persistence.cache.storeMode", "REFRESH")
                .setParameter("iduser", u)
                .getResultList().stream().forEach((p) -> {
                    projectItems.add(p);
                });
        coldatedebutproject.setCellValueFactory((TableColumn.CellDataFeatures<Project, String> param) -> new ReadOnlyObjectWrapper<>(param.getValue().getDatedebut()));
        coldatefinproject.setCellValueFactory((TableColumn.CellDataFeatures<Project, String> param) -> new ReadOnlyObjectWrapper<>(param.getValue().getDatefin()));
        colnameproject.setCellValueFactory((TableColumn.CellDataFeatures<Project, String> param) -> new ReadOnlyObjectWrapper<>(param.getValue().getName()));
        colnameproject.setCellValueFactory((TableColumn.CellDataFeatures<Project, String> param) -> new ReadOnlyObjectWrapper<>(param.getValue().getName()));
        coldescrproject.setCellValueFactory((TableColumn.CellDataFeatures<Project, String> param) -> new ReadOnlyObjectWrapper<>(param.getValue().getDescription()));
        colfinishproject.setCellValueFactory((TableColumn.CellDataFeatures<Project, String> param) -> new ReadOnlyObjectWrapper<>(param.getValue().getFinish()));
        coldifficultyproject.setCellValueFactory((TableColumn.CellDataFeatures<Project, String> param) -> new ReadOnlyObjectWrapper<>(param.getValue().getIddf().getName()));
        coltypeproject.setCellValueFactory((TableColumn.CellDataFeatures<Project, String> param) -> new ReadOnlyObjectWrapper<>(param.getValue().getIdtp().getName()));

        FilteredList<Project> filteredList = new FilteredList<>(projectItems, p -> true);
        txtsearch.textProperty().addListener((ObservableValue<? extends String> observable, String oldValue, String newValue) -> {
            filteredList.setPredicate((Project t) -> {
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }
                String lowerCaseFilter = newValue.toLowerCase();

                if (t.getName().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                return false;
            });
        });
        SortedList<Project> sortedData = new SortedList<>(filteredList);
        sortedData.comparatorProperty().bind(tableProject.comparatorProperty());
        tableProject.setItems(sortedData);
    }

    private void initTableFunc(Project pr) {
        tablefunc.getItems().clear();
        functItems.clear();
        if (pr != null) {
            em.createNamedQuery("Fonctionality.findAllByproject", Fonctionality.class)
                    .setHint("javax.persistence.cache.storeMode", "REFRESH")
                    .setParameter("idprojet", pr)
                    .getResultList().stream().forEach((p) -> {

                        functItems.add(p);

                    });
            coldatedebutfonc.setCellValueFactory((TableColumn.CellDataFeatures<Fonctionality, String> param) -> new ReadOnlyObjectWrapper<>(param.getValue().getDateHeurDebut()));
            coldatefinfonc.setCellValueFactory((TableColumn.CellDataFeatures<Fonctionality, String> param) -> new ReadOnlyObjectWrapper<>(param.getValue().getDateHeurFin()));
            coldescfonc.setCellValueFactory((TableColumn.CellDataFeatures<Fonctionality, String> param) -> new ReadOnlyObjectWrapper<>(param.getValue().getDescription()));
            colnamefonct.setCellValueFactory((TableColumn.CellDataFeatures<Fonctionality, String> param) -> new ReadOnlyObjectWrapper<>(param.getValue().getName()));
            colfinishfonc.setCellValueFactory((TableColumn.CellDataFeatures<Fonctionality, String> param) -> new ReadOnlyObjectWrapper<>(param.getValue().getFinish()));

            tablefunc.setItems(functItems);
        }

    }

    private ImageView setIcon(String img) {
        Image openIcon = new Image(img);
        ImageView openView = new ImageView(openIcon);
        openView.setFitWidth(20);
        openView.setFitHeight(20);

        return openView;
    }

    private void ContextMenuProject() {
        ContextMenu cm = new ContextMenu();
        MenuItem addproject = new MenuItem("Nouveau projet");

        addproject.setGraphic(setIcon("resource/img/add.jpg"));
        addproject.setOnAction((ActionEvent event) -> {
            AddProject addProject = new AddProject(user);
            initTableProject(user);
        });

        MenuItem editproject = new MenuItem("editer le projet");
        editproject.setGraphic(setIcon("resource/img/edit.jpg"));
        editproject.setOnAction((ActionEvent event) -> {
            if (tableProject.getSelectionModel().getSelectedItem() != null) {
                Editproject e = new Editproject(tableProject.getSelectionModel().getSelectedItem());
                initTableProject(user);
            }
        });

        MenuItem addFonctionality = new MenuItem("Nouvelle fonctionnalité");
        addFonctionality.setGraphic(setIcon("resource/img/addFonc.jpg"));
        addFonctionality.setOnAction((ActionEvent event) -> {
            if (tableProject.getSelectionModel().getSelectedItem() != null) {
                FonctionnalityDialog fd = new FonctionnalityDialog(tableProject.getSelectionModel().getSelectedItem(), null, "add");
                initTableFunc(tableProject.getSelectionModel().getSelectedItem());
            }
        });

        MenuItem finish = new MenuItem("marqué Fini");
        finish.setGraphic(setIcon("resource/img/finish.jpg"));
        finish.setOnAction((ActionEvent event) -> {
            if (tableProject.getSelectionModel().getSelectedItem() != null) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("confirmation?");
                alert.setContentText("etes-vous sur de vouloir marqué ce projet finit?");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK) {
                    ProjectJpaController pjc = new ProjectJpaController(emf());
                    Project p = tableProject.getSelectionModel().getSelectedItem();
                    Project project = new Project(p.getIdprojet(), p.getName(), p.getDescription(), p.getDatedebut(), p.getDatefin(), "oui", p.getIddf(), p.getIdtp(), p.getIduser());
                    try {
                        pjc.edit(project);
                        OnRefresh(null);
                        initTableProject(user);
                    } catch (Exception ex) {
                        Alert(ex, Alert.AlertType.ERROR, "veuillez remplir les champ correctement svp");
                        ex.printStackTrace();
                    }
                }
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("ERROR");
                alert.setContentText("Aucun projet selectionné");
                Optional<ButtonType> result = alert.showAndWait();

            }
        });

        cm.getItems().addAll(addproject, addFonctionality, editproject, finish);
        tableProject.setContextMenu(cm);
    }

    private void ContextMenuFonctionnality() {
        ContextMenu cm = new ContextMenu();

        MenuItem editproject = new MenuItem("editer la fonctionnalité");
        editproject.setGraphic(setIcon("resource/img/edit.jpg"));
        editproject.setOnAction((ActionEvent event) -> {
            if (tablefunc.getSelectionModel().getSelectedItem() != null) {
                FonctionnalityDialog fd = new FonctionnalityDialog(tableProject.getSelectionModel().getSelectedItem(), tablefunc.getSelectionModel().getSelectedItem(), "edit");
                initTableFunc(tableProject.getSelectionModel().getSelectedItem());
            }
        });

        MenuItem addFonctionality = new MenuItem("Nouvelle fonctionnalité");
        addFonctionality.setGraphic(setIcon("resource/img/addFonc.jpg"));
        addFonctionality.setOnAction((ActionEvent event) -> {
            if (tableProject.getSelectionModel().getSelectedItem() != null) {
                FonctionnalityDialog fd = new FonctionnalityDialog(tableProject.getSelectionModel().getSelectedItem(), null, "add");
                initTableFunc(tableProject.getSelectionModel().getSelectedItem());
            }
        });
        MenuItem finish = new MenuItem("marqué Fini");
        //finish.setGraphic(setIcon("resource/img/finish.jpg"));
        finish.setOnAction((ActionEvent event) -> {
            if (tableProject.getSelectionModel().getSelectedItem() != null) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("confirmation");
                alert.setContentText("etes-vous sur de vouloir marqué cette fonctionnalitée fini?");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK) {
                    FonctionalityJpaController pjc = new FonctionalityJpaController(emf());
                    Fonctionality f = tablefunc.getSelectionModel().getSelectedItem();
                    Fonctionality project = new Fonctionality(f.getIdfonc(), f.getName(), f.getDescription(), f.getDateHeurDebut(), f.getDateHeurFin(), f.getIdprojet(), "OUI");
                    try {
                        pjc.edit(project);
                        OnRefresh(null);
                        initTableFunc(tableProject.getSelectionModel().getSelectedItem());
                    } catch (Exception ex) {
                        Alert(ex, Alert.AlertType.ERROR, "veuillez remplir les champ correctement svp");
                        ex.printStackTrace();
                    }
                }
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("ERROR");
                alert.setContentText("Aucun projet selectionné");
                Optional<ButtonType> result = alert.showAndWait();

            }
        });
        cm.getItems().addAll(addFonctionality, editproject, finish);
        tablefunc.setContextMenu(cm);
    }

    public void setUser(User user) {
        initTableProject(user);
        this.user = user;
    }

    @FXML
    private void Ondifficulty(ActionEvent event) {
        DifficultyDialog dd = new DifficultyDialog();
    }

    @FXML
    private void OnAddproject(ActionEvent event) {
        AddProject addProject = new AddProject(user);
    }

    @FXML
    private void OnAddfonct(ActionEvent event) {
        if (tableProject.getSelectionModel().getSelectedItem() != null) {
            FonctionnalityDialog fd = new FonctionnalityDialog(tableProject.getSelectionModel().getSelectedItem(), null, "add");
            initTableFunc(tableProject.getSelectionModel().getSelectedItem());
        }
    }

    @FXML
    private void OnEditProject(ActionEvent event) {
        if (tableProject.getSelectionModel().getSelectedItem() != null) {
            Editproject e = new Editproject(tableProject.getSelectionModel().getSelectedItem());
            initTableProject(user);
        }
    }

    @FXML
    private void OnEditFonc(ActionEvent event) {
        if (tablefunc.getSelectionModel().getSelectedItem() != null) {
            FonctionnalityDialog fd = new FonctionnalityDialog(tableProject.getSelectionModel().getSelectedItem(), tablefunc.getSelectionModel().getSelectedItem(), "edit");
            initTableFunc(tableProject.getSelectionModel().getSelectedItem());
        }
    }

    @FXML
    private void OnSetting(ActionEvent event) {

        try {
            FXMLLoader load = new FXMLLoader(getClass().getResource("/FXML/SettingFXML.fxml"));
            Parent Root;
            Root = load.load();
            Stage stage = new Stage();
            stage.setScene(new Scene(Root));
            SettingFXMLController controller = (SettingFXMLController) load.getController();
            controller.setUser(user);
            stage.showAndWait();
        } catch (IOException ex) {
            Logger.getLogger(MainFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
