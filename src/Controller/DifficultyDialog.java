/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DB.DifficultyLevelJpaController;
import Entity.DifficultyLevel;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

/**
 * @author ROBLEH HOUSSEIN FARAH
 */
public class DifficultyDialog extends Dialog implements ILoad {

    public DifficultyDialog() {
        INITLevel();
    }

    private void INITLevel() {
        Dialog dialog = new Dialog();
        dialog.setTitle("Niveau & Difficulté");
        dialog.setHeaderText("Niveau & Difficulté");
        ButtonType cancel = new ButtonType("Fermer", ButtonBar.ButtonData.CANCEL_CLOSE);

        GridPane gp = new GridPane();
        gp.setHgap(10);
        gp.setVgap(10);
        gp.setPadding(new Insets(20, 150, 10, 10));

        HBox hBox = new HBox();

        Label name = new Label("Nom");
        TextField txtname = new TextField();
        gp.add(name, 0, 0);
        gp.add(txtname, 1, 0);

        Label desc = new Label("point");
        TextField txtpoint = new TextField();

        txtpoint.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.isEmpty()) {
                    try {
                        long pointI = Integer.parseInt(newValue);
                        txtpoint.setText(String.valueOf(pointI));
                    } catch (Exception e) {
                        txtpoint.clear();
                        txtpoint.setText(getNumber(oldValue));
                    }
                }
            }
        });
        gp.add(desc, 0, 1);
        gp.add(txtpoint, 1, 1);

        Button save = new Button("Enregistrer");
        save.setOnAction((ActionEvent event) -> {
            if (!txtname.getText().equals("") && !txtpoint.getText().equals("")) {
                DifficultyLevel dl = null;
                try {
                    dl = em().createNamedQuery("DifficultyLevel.findByName", DifficultyLevel.class).setParameter("name", txtname.getText()).getSingleResult();
                } catch (Exception e) {
                    System.out.println("unfounded");
                }
                if (dl == null) {
                    DifficultyLevelJpaController dljc = new DifficultyLevelJpaController(emf());
                    dljc.create(new DifficultyLevel(Integer.SIZE, txtname.getText(), Integer.parseInt(txtpoint.getText())));
                } else {
                    Alert(new Exception("un niveau de difficulté de ce nom existe deja"), Alert.AlertType.ERROR, "un niveau de difficulté de ce nom existe deja");
                }
            } else {
                Alert(new Exception("veuillez remplir les champs correctement"), Alert.AlertType.ERROR, "veuillez remplir les champs correctement");
            }
        });
        Button clear = new Button("Effacer");
        clear.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                txtpoint.clear();
                txtname.clear();
            }
        });
        gp.add(save, 0, 2);
        gp.add(clear, 1, 2);

        hBox.getChildren().addAll(gp, table());
        dialog.getDialogPane().getButtonTypes().addAll(cancel);
        dialog.getDialogPane().setContent(hBox);

        
        dialog.showAndWait();
    }

    private Node table() {
        TableView<Entity.DifficultyLevel> table = new TableView<>();
        ObservableList<Entity.DifficultyLevel> items = FXCollections.observableArrayList();
        TableColumn<Entity.DifficultyLevel, String> name = new TableColumn<>("Nom");
        TableColumn<Entity.DifficultyLevel, Integer> point = new TableColumn<>("Point");

        em().createNamedQuery("DifficultyLevel.findAll", DifficultyLevel.class).setHint("javax.persistence.cache.storeMode", "REFRESH").getResultList().stream().forEach((d) -> {
            items.add(d);
        });
        name.setCellValueFactory((TableColumn.CellDataFeatures<DifficultyLevel, String> param) -> new ReadOnlyObjectWrapper(param.getValue().getName()));
        point.setCellValueFactory((TableColumn.CellDataFeatures<DifficultyLevel, Integer> param) -> new ReadOnlyObjectWrapper<>(param.getValue().getPoint()));
        table.getColumns().addAll(name, point);
        table.setItems(items);
        return table;
    }

    private String getNumber(String value) {
        String n = "";
        try {
            return String.valueOf(Integer.parseInt(value));
        } catch (Exception e) {
            String[] array = value.split("");
            for (String tab : array) {
                try {
                    System.out.println(tab);
                    n = n.concat(String.valueOf(Integer.parseInt(String.valueOf(tab))));
                } catch (Exception ex) {
                    System.out.println("not nomber");
                }
            }
            return n;
        }
    }
}
