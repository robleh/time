package Controller;

import DB.FonctionalityJpaController;
import Entity.Fonctionality;
import Entity.Project;
import java.time.format.DateTimeFormatter;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

/**
 * @author ROBLEH HOUSSEIN FARAH
 */
public class FonctionnalityDialog extends Dialog implements ILoad {

    

    public FonctionnalityDialog(Project p, Fonctionality f, String option) {
        if ("edit".equals(option)) {
            edit(f, option);
        } else if (option.equals("add")) {
            add(p);
        }
    }

    private void edit(Fonctionality f, String option) {
        Dialog dialog = new Dialog();
        dialog.setTitle("Modification de la fonctionnalité " + f.getName());
        dialog.setHeaderText("Modification de la fonctionnalité " + f.getName());
        ButtonType ok = new ButtonType("Enregistrer", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Annuler", ButtonBar.ButtonData.CANCEL_CLOSE);

        GridPane gp = new GridPane();
        gp.setHgap(10);
        gp.setVgap(10);

        Label name = new Label("Nom");
        TextField txtname = new TextField(f.getName());
        gp.add(name, 0, 0);
        gp.add(txtname, 1, 0);

        Label desc = new Label("Description");
        TextField txtdesc = new TextField(f.getDescription());
        gp.add(desc, 0, 1);
        gp.add(txtdesc, 1, 1);

        Label datedeb = new Label("Date début");
        DatePicker dateDebut = new DatePicker();
        gp.add(datedeb, 0, 2);
        gp.add(dateDebut, 1, 2);

        Label datefin = new Label("Date fin");
        DatePicker datfin = new DatePicker();
        gp.add(datefin, 0, 3);
        gp.add(datfin, 1, 3);

        dialog.getDialogPane().getButtonTypes().addAll(ok, cancel);
        dialog.getDialogPane().setContent(gp);

        dialog.setResultConverter((param) -> {
            if (param == ok) {
                try {
                    FonctionalityJpaController fjc = new FonctionalityJpaController(emf());
                    if (dateDebut.getValue().format(DateTimeFormatter.ISO_DATE).isEmpty() && dateDebut.getValue().format(DateTimeFormatter.ISO_DATE).isEmpty()) {
                        System.out.println("date vite");
                        fjc.edit(new Fonctionality(f.getIdfonc(), txtname.getText(), txtdesc.getText(), f.getDateHeurDebut(),
                                f.getDateHeurFin(), f.getIdprojet(),f.getFinish()));
                    } else {
                        fjc.edit(new Fonctionality(f.getIdfonc(), txtname.getText(), txtdesc.getText(), dateDebut.getValue().format(DateTimeFormatter.ISO_LOCAL_DATE),
                                datfin.getValue().format(DateTimeFormatter.ISO_LOCAL_DATE), f.getIdprojet(),f.getFinish()));
                    }
                } catch (Exception e) {
                    Alert(e, Alert.AlertType.ERROR, "veuillez remplir les champ correctement svp");
                    if (dialog.isShowing()) {
                        dialog.showAndWait();
                    }
                }

            } else {
                dialog.close();
            }
            return null;
        });
        dialog.showAndWait();
    }

    private void add(Project p) {
        Dialog dialog = new Dialog();
        dialog.setTitle("Nouvelle fonctionnalité");
        dialog.setHeaderText("Nouvelle fonctionnalité");
        ButtonType ok = new ButtonType("Enregistrer", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Annuler", ButtonBar.ButtonData.CANCEL_CLOSE);

        GridPane gp = new GridPane();
        gp.setHgap(10);
        gp.setVgap(10);

        Label name = new Label("Nom");
        TextField txtname = new TextField();
        gp.add(name, 0, 0);
        gp.add(txtname, 1, 0);

        Label desc = new Label("Description");
        TextField txtdesc = new TextField();
        gp.add(desc, 0, 1);
        gp.add(txtdesc, 1, 1);

        Label datedeb = new Label("Date début");
        DatePicker dateDebut = new DatePicker();
        gp.add(datedeb, 0, 2);
        gp.add(dateDebut, 1, 2);

        Label datefin = new Label("Date fin");
        DatePicker datfin = new DatePicker();
        gp.add(datefin, 0, 3);
        gp.add(datfin, 1, 3);

        dialog.getDialogPane().getButtonTypes().addAll(ok, cancel);
        dialog.getDialogPane().setContent(gp);

        dialog.setResultConverter((param) -> {
            if (param == ok) {
                try {
                    FonctionalityJpaController fjc = new FonctionalityJpaController(emf());
                    fjc.create(new Fonctionality(Integer.SIZE, txtname.getText(), txtdesc.getText(), dateDebut.getValue().format(DateTimeFormatter.ISO_LOCAL_DATE),
                            datfin.getValue().format(DateTimeFormatter.ISO_LOCAL_DATE), p,"NON"));
                } catch (Exception e) {
                    Alert(e, Alert.AlertType.ERROR, "veuillez remplir les champ correctement svp");
                    if (dialog.isShowing()) {
                        dialog.showAndWait();
                    }
                }

            } else {
                dialog.close();
            }
            return null;
        });
        dialog.showAndWait();
    }

}
