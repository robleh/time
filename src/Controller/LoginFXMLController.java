package Controller;

import Entity.User;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class LoginFXMLController implements Initializable, ILoad {

    @FXML
    private TextField txtLogin;
    @FXML
    private TextField txtpwd;

    private final EntityManagerFactory emf = Persistence.createEntityManagerFactory("TimePU");

    private EntityManager em = emf.createEntityManager();

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    @FXML
    private void OnConnect(ActionEvent event) {
        if (!txtLogin.getText().isEmpty() && !txtpwd.getText().isEmpty()) {
            User user = null;
            try {
                user = em.createNamedQuery("User.findByLogin", User.class)
                        .setParameter("login", txtLogin.getText())
                        .setParameter("password", txtpwd.getText())
                        .getSingleResult();
            } catch (Exception e) {
                Alert(new Exception("combinaison incorrect"), Alert.AlertType.ERROR, "combinaison incorrect veuillez réessayer a nouveau");
            }

            if (user != null && user.getLogin().equals(txtLogin.getText()) && user.getPassword().equals(txtpwd.getText())) {
                try {
                    FXMLLoader load = new FXMLLoader(getClass().getResource("/FXML/MainFXML.fxml"));
                    Parent Root = load.load();
                    Stage stage = new Stage();
                    stage.setScene(new Scene(Root));
                    MainFXMLController controller = load.getController();
                    controller.setUser(user);
                    stage.setTitle("PROJECT ET FONCTIONNALITE");
                    stage.show();
                    Button btn = (Button) event.getSource();
                    btn.getScene().getWindow().hide();
                } catch (Exception e) {
                    Alert(new Exception(" combinaison incorrect"), Alert.AlertType.ERROR, "combinaison incorrect");
                    //e.printStackTrace();
                }
            }
        } else {
            Alert(new Exception("erreur: combinaison incorrect"), Alert.AlertType.ERROR, "ERREUR: combinaison incorrect");
            System.out.println("Error");
        }
    }

    @FXML
    private void OnCancel(ActionEvent event) {
        Button btn = (Button) event.getSource();
        btn.getScene().getWindow().hide();
    }

}
