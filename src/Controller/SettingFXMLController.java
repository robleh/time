/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DB.UserJpaController;
import Entity.User;
import Main.TimeG;
import java.awt.Window;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.management.ManagementFactory;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Robleh
 */
public class SettingFXMLController implements Initializable, ILoad {

    @FXML
    private TextField txtlogin;
    @FXML
    private TextArea txtmot;
    @FXML
    private PasswordField txtpwd;
    @FXML
    private TextField txtAreafb;
    @FXML
    private CheckBox checkAll;
    @FXML
    private CheckBox checkNotFinish;
    @FXML
    private CheckBox checkFinish;
    @FXML
    private CheckBox checkFirst;
    @FXML
    private CheckBox checkToday;

    Properties prop = new Properties();
    FileInputStream in = null;
    private User user = null;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        initProperties();
        try {
            in = new FileInputStream(Paths.get("config.properties").toAbsolutePath().toFile().getName());
            System.err.println("LOADED");
            System.out.println(in.read());
            prop.load(in);

            if (prop.get("Today").equals("true")) {
                checkToday.setSelected(true);
            }
            if (prop.get("First").equals("true")) {
                checkFirst.setSelected(true);
            }
            if (prop.get("Finish").equals("true")) {
                checkFinish.setSelected(true);
            }
            if (prop.get("Notfinish").equals("true")) {
                checkNotFinish.setSelected(true);
            }
            if (prop.get("All").equals("true")) {
                checkAll.setSelected(true);
            }
        } catch (IOException io) {
            initProperties();
            io.printStackTrace();
        }
    }

    private void setproperties(String key, String value) {
        Properties properties = new Properties();
        try {
            InputStream input = new FileInputStream(Paths.get("config.properties").toAbsolutePath().toFile().getName());
            properties.load(input);
            if (properties.containsKey(key)) {
                properties.setProperty(key, value);
                properties.setProperty("All", "false");
            }
            OutputStream ou = new FileOutputStream(Paths.get("config.properties").toAbsolutePath().toFile().getName());
            properties.store(ou, null);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SettingFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SettingFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void initProperties() {
        Properties properties = new Properties();
        try {

            InputStream in = new FileInputStream(Paths.get("config.properties").toAbsolutePath().toFile().getName());
            properties.load(in);
            if (!properties.containsKey("Today")) {
                properties.setProperty("Today", "false");
            }
            if (!properties.containsKey("Finish")) {
                properties.setProperty("Finish", "false");
            }
            if (!properties.containsKey("First")) {
                properties.setProperty("First", "false");
            }
            if (!properties.containsKey("Notfinish")) {
                properties.setProperty("Notfinish", "false");
            }
            if (!properties.containsKey("All")) {
                properties.setProperty("All", "true");
            }
            OutputStream oupt = new FileOutputStream(Paths.get("config.properties").toAbsolutePath().toFile().getName());
            properties.store(oupt, null);

        } catch (FileNotFoundException ex) {
            try {
                OutputStream oupt = new FileOutputStream(Paths.get("config.properties").toAbsolutePath().toFile().getName());
                initProperties();
            } catch (FileNotFoundException ex1) {
                Logger.getLogger(SettingFXMLController.class.getName()).log(Level.SEVERE, null, ex1);
            }
            // Logger.getLogger(SettingFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SettingFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void OnUpdate(ActionEvent event) {
        UserJpaController controller = new UserJpaController(emf());
        if (user != null) {
            if (!txtlogin.getText().isEmpty() && !txtpwd.getText().isEmpty()) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setContentText("etes-vous sur de vouloir changé votre combinaison et de redemarrer le programme");
                Optional<ButtonType> result = alert.showAndWait();
                if (result.get() == ButtonType.OK) {
                    try {
                        try {
                            controller.edit(new User(user.getIduser(), user.getName(), user.getSexe(), txtlogin.getText(), txtpwd.getText()));
                        } catch (Exception ex) {
                            Logger.getLogger(SettingFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        restart();
                    } catch (IOException ex) {
                        Logger.getLogger(SettingFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }

            }
        }
    }

    private void restart() throws IOException {
                String SUN_JAVA_COMMAND = "sun.java.command";
        try {
// java binary
            String java = System.getProperty("java.home") + "/bin/java";
// vm arguments
            List<String> vmArguments = ManagementFactory.getRuntimeMXBean().getInputArguments();
            StringBuffer vmArgsOneLine = new StringBuffer();
            for (String arg : vmArguments) {
// if it's the agent argument : we ignore it otherwise the
// address of the old application and the new one will be in conflict
                if (!arg.contains("-agentlib")) {
                    vmArgsOneLine.append(arg);
                    vmArgsOneLine.append(" ");
                }
            }
// init the command to execute, add the vm args
            final StringBuffer cmd = new StringBuffer("\"" + java + "\" " + vmArgsOneLine);

// program main and program arguments
            String[] mainCommand = System.getProperty(SUN_JAVA_COMMAND).split(" ");

// program main is a jar
            if (mainCommand[0].endsWith(".jar")) {
// if it's a jar, add -jar mainJar
                cmd.append("-jar ").append(new File(mainCommand[0]).getPath());
            } else {
// else it's a .class, add the classpath and mainClass
                cmd.append("-cp \"").append(System.getProperty("java.class.path")).append("\" ").append(mainCommand[0]);
            }
// finally add program arguments
            for (int i = 1; i < mainCommand.length; i++) {
                cmd.append(" ");
                cmd.append(mainCommand[i]);
            }
            System.out.println(cmd);
// resources have been disposed before restarting the application
            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    try {
                        Runtime.getRuntime().exec(cmd.toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
// exit
            System.exit(0);
        } catch (Exception e) {
// something went wrong
            throw new IOException("Error while trying to restart the application", e);
        }
    }

    @FXML
    private void checkNotFinish(ActionEvent event) {
        if (checkNotFinish.isSelected()) {
            setproperties("Notfinish", "true");
            checkToday.setSelected(false);
            checkFirst.setSelected(false);
            checkFinish.setSelected(false);
            checkAll.setSelected(false);
            setproperties("Today", "false");
            setproperties("First", "false");
            setproperties("Finish", "false");
        }

    }

    @FXML
    private void checkFnish(ActionEvent event) {
        if (checkFinish.isSelected()) {
            setproperties("Finish", "true");
            checkToday.setSelected(false);
            checkFirst.setSelected(false);
            checkNotFinish.setSelected(false);
            checkAll.setSelected(false);
            setproperties("Today", "false");
            setproperties("First", "false");
            setproperties("Notfinish", "false");
        }
    }

    @FXML
    private void checkFirst(ActionEvent event) {
        if (checkFirst.isSelected()) {
            setproperties("First", "true");
            checkToday.setSelected(false);
            checkNotFinish.setSelected(false);
            checkNotFinish.setSelected(false);
            checkAll.setSelected(false);
            setproperties("Today", "false");
            setproperties("Finish", "false");
            setproperties("Notfinish", "false");

        }
    }

    @FXML
    private void checkToDay(ActionEvent event) {
        if (checkToday.isSelected()) {
            setproperties("Today", "true");
            checkFirst.setSelected(false);
            checkNotFinish.setSelected(false);
            checkNotFinish.setSelected(false);
            checkAll.setSelected(false);
            setproperties("First", "false");
            setproperties("Finish", "false");
            setproperties("Notfinish", "false");
        }
    }

    @FXML
    private void OnFeeback(ActionEvent event) {
    }

    public void setUser(User u) {
        if (u != null) {
            txtlogin.setText(u.getLogin());
            txtpwd.setText(u.getPassword());
            user = u;
        }

    }

}
