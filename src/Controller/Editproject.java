
package Controller;

import DB.ProjectJpaController;
import Entity.DifficultyLevel;
import Entity.Project;
import java.time.format.DateTimeFormatter;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.util.Callback;

/**
 *
 * @author ROBLEH HOUSSEIN FARAH
 */
class Editproject extends Dialog implements ILoad {

    private DifficultyLevel level = null;

    Editproject(Project selectedItem) {
        init(selectedItem);
    }

    private void init(Project selectedItem) {
        Dialog dialog = new Dialog();
        dialog.setTitle("Mettre à jour ");
        dialog.setHeaderText("Mettre à jour t");
        ButtonType ok = new ButtonType("Enregistrer", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Annuler", ButtonBar.ButtonData.CANCEL_CLOSE);
        level = selectedItem.getIddf();
        
        GridPane gp = new GridPane();
        gp.setHgap(10);
        gp.setVgap(10);
       // gp.setPadding(new Insets(20, 150, 10, 10));

        Label name = new Label("Nom");
        TextField txtname = new TextField(selectedItem.getName());
        gp.add(name, 0, 0);
        gp.add(txtname, 1, 0);

        Label desc = new Label("Description");
        TextField txtdesc = new TextField(selectedItem.getDescription());
        gp.add(desc, 0, 1);
        gp.add(txtdesc, 1, 1);

        Label difficulty = new Label("niveau de difficulté");
        ComboBox<String> combodifficulty = new ComboBox();

        em().createNamedQuery("DifficultyLevel.findAll", DifficultyLevel.class).getResultList().stream().forEach((d) -> {
            combodifficulty.getItems().add(d.getName());
        });
        combodifficulty.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                level = em().createNamedQuery("DifficultyLevel.findByName", DifficultyLevel.class).setParameter("name", newValue).getSingleResult();
                System.out.println(newValue);
            }
        });

        gp.add(difficulty, 0, 2);
        gp.add(combodifficulty, 1, 2);

        Label datedeb = new Label("Date début");
        DatePicker dateDebut = new DatePicker();
        gp.add(datedeb, 0, 3);
        gp.add(dateDebut, 1, 3);

        Label datefin = new Label("Date fin");
        DatePicker datfin = new DatePicker();
        gp.add(datefin, 0, 4);
        gp.add(datfin, 1, 4);

        dialog.getDialogPane().getButtonTypes().addAll(ok, cancel);
        dialog.getDialogPane().setContent(gp);

       
        dialog.setResultConverter(new Callback<ButtonType, ButtonType>() {
            @Override
            public ButtonType call(ButtonType param) {
                if (param == ok) {
                    try {
                        ProjectJpaController pjc = new ProjectJpaController(emf());

                        if (dateDebut.getValue() != null && datfin.getValue() != null && level != null) {
                            Project p = new Project(selectedItem.getIdprojet(), txtname.getText(), txtdesc.getText(), dateDebut.getValue().format(DateTimeFormatter.ISO_DATE),
                                    datfin.getValue().format(DateTimeFormatter.ISO_DATE), selectedItem.getFinish(), level, selectedItem.getIdtp(), selectedItem.getIduser());
                            pjc.edit(p);
                             Alert(new Exception("success"),Alert.AlertType.INFORMATION,"Enregistrer avec succès MERCI");
                        } else {
                            Project p = new Project(selectedItem.getIdprojet(), txtname.getText(), txtdesc.getText(), selectedItem.getDatedebut(),
                                    selectedItem.getDatefin(), selectedItem.getFinish(), level, selectedItem.getIdtp(), selectedItem.getIduser());
                            pjc.edit(p);
                             Alert(new Exception("success"),Alert.AlertType.INFORMATION,"Enregistrer avec succès MERCI");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Alert(e,Alert.AlertType.ERROR,"veuillez remplir les champ correctement svp");
                        if (dialog.isShowing()) {
                            dialog.showAndWait();
                        }
                    }

                } else {
                    dialog.close();
                }
                return null;
            }
        });
        dialog.showAndWait();
    }

}
