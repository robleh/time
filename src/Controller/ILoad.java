/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author ROBLEH HOUSSEIN FARAH
 */
public interface ILoad {

    public default EntityManagerFactory emf() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TimePU");
        EntityManager em = emf.createEntityManager();
        return emf;
    }

    public default EntityManager em() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("TimePU");
        EntityManager em = emf.createEntityManager();
        return em;
    }

    public default void Alert(Exception e, AlertType type, String msg) {
        Alert alert = new Alert(type);
        alert.setTitle(e.getMessage());
        alert.setContentText(msg + " \n" + e.toString());
        alert.showAndWait();
    }

    public default String date() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String d = dateFormat.format(date);
        System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
        return dateFormat.format(date);
    }

}
